#!/usr/bin/env bash

mkdocs build
rsync -avzhe ssh ./site/ keymaker:../../var/www/html
