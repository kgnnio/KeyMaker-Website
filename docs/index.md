# KeyMaker Browser Extension
A browser extension which will automate away all your password management complexities, while ensuring your account websites are always secure. The security aspects of password management are based on the [Master Password algorithm](http://masterpasswordapp.com/), which is a [deterministic password generation](http://www.ghacks.net/2016/10/03/password-managers-that-dont-store-passwords/) algorithm.

## Features
### Seamless management of your account meta data.
Your account meta data is comprised of some basic information about your account, and does not include your actual passwords. Rather than that, this information is used to generate passwords with the addition of the Master Password, which only the user knows. For this reason, there is no risk of your passwords getting leaked in the case some one comes in possession of this meta data.

The following is an example of a users meta data. Note that the actual passwords are not stored, ensuring that your passwords will not be breached in the case someone comes in possession of this data. For this reason, one may decide that it would be safe to sync this file with 3rd party synchronization services such as DropBox or Google Drive.

```
{
  "accounts": [
    {
      "counter": 1,
      "id": 1,
      "name": "amazon.com",
      "passwordType": 0,
      "uri": "www.amazon.com",
      "user": "my@email.com"
    },
    {
      "counter": 1,
      "id": 2,
      "name": "ebay.com",
      "passwordType": 0,
      "uri": "www.ebay.com",
      "user": "my@email.com"
    }
    ],
  "nextId": 3
}
```

### Automatic input of your passwords whenever you encounter a website which you have saved an account for.
KeyMaker will prompt you whenever you encounter a page for which you have registered an account.

![Auto Input](./img/auto_input.png)

### Semi-automatic registration of new websites to your KeyMaker accounts.
You will be prompted whether you would want to register a website to the KeyMaker database, if it you have not.

#### Ask if you want to register a certain website in the KeyMaker database.
![Ask Register](./img/ask_register.png)
#### Create Account interface
![Create Account](./img/create_account.png)
#### Accounts interface
![Accounts](./img/accounts.png)

## About the Master Password algorithm
Please refer to [Master Password algorithm](http://masterpasswordapp.com/) for details.

## How to install
Currently only Chrome on Windows, Linux and macOS are supported. The extension is supported by back-end software which handles the actual password generation and meta data management. Both must be installed for the extension to be functionaly

### Chrome
Coming soon...

### Firefox
A Firefox version is planned.

### Linux
### Windows
### macOS

## FAQ
Coming soon...

## Disclaimers
Please acknowledge the following.

- This software is still in alpha state, and the database format may be changed.

- Account meta-data is currently not encrypted. This is planned to be implemented in the near future.

- The current KeyMaker implementation deviates from the Master Password algorithm in that it adds three "No Symbol" versions of password types. This is because there are many websites where their password policy does not permit symbols(e.g. !@#$%^&).

- Copy and pasting of passwords is inherently insecure, as it exposes passwords to all other software, black or white. This feature is added as a convenience. However, the auto input feature should preferred.

## Implementation details
The extension relies on the [NativeMessaging API](https://developer.chrome.com/extensions/nativeMessaging), which interacts with a back-end application. This was required since it is the only way a browser may interact with a file on the user's PC. All password generation and meta-data is handled by the back-end, and the extension's functionality is limited to the user interface.

## Credits
* Maarten Billemont for the [Master Password algorithm](http://masterpasswordapp.com/).
* Stefan Scherfke for letting me reuse his Master Password algorithm implementation, [mpw](https://pypi.python.org/pypi/mpw/).
* 文翼(wenzhixin) for [bootstrap-table](https://github.com/wenzhixin/bootstrap-table).
* Anton Romanovich for [jsl](https://github.com/aromanovich/jsl).
* Julian Berman for [jsonschema](https://github.com/Julian/jsonschema).
* Al Sweigart for [pyperclip](https://github.com/asweigart/pyperclip).
